from string import Template

def clean_plotly_html(txt):
    """ clean firsts and last lines from a txt string

        - input: string with content from a plotly generated html
        - output: cleaned string
    """
    pass

# template de lectura y archivo de salida
index_output = open("web/index2.html", "w")
template_html = open("web/index_template2.html", "r").read()
template_tmp  = Template(template_html)

# figuras generadas con plotly y purgadas por ahora a mano -> nada mas que sacar el html y body
totales_paises         = open("graphs/totales_paises.html", "r").read()
barras_provincias_anim = open("graphs/barras_provincias_anim.html", "r").read()
total_arg_provincias   = open("graphs/total_arg_provincias.html", "r").read()
mapa_sfe               = open("graphs/mapa_sfe.html", "r").read()
mapa_mundo_anim        = open("graphs/mapa_mundo_anim.html", "r").read()

index_content = template_tmp.substitute(totales_paises          = totales_paises,
                                        barras_provincias_anim  = barras_provincias_anim,
                                        total_arg_provincias    = total_arg_provincias,
                                        mapa_sfe                = mapa_sfe,
                                        mapa_mundo_anim         = mapa_mundo_anim,
                                        )
index_output.write(index_content)
index_output.close()